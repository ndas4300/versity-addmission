<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUtmeResultCheckersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utme_result_checkers', function (Blueprint $table) {
            $table->id();
            $table->string('student_name');
            $table->string('admission_number');
            $table->foreignId('department_id')->constrained('departments')->onDelete('cascade');
            $table->string('phone_number');
            $table->string('email');
            $table->foreignId('fee_id')->constrained('fees')->onDelete('cascade');
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->string('token')->unique();
            $table->boolean('payment_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utme_result_checkers');
    }
}
