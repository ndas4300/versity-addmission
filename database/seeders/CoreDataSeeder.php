<?php

namespace Database\Seeders;

use App\Models\CompanySetting;
use App\Models\Department;
use App\Models\Fee;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CoreDataSeeder extends Seeder
{
    public function run()
    {
        $this->createDefaultSettings();
        $this->createDefaultRolePermissions();
        $this->createDefaultUsers();
        $this->createDefaultFees();
        $this->createDefaultDepartments();
    }

    private function createDefaultSettings()
    {
        CompanySetting::create([
            'name' => 'Calabar University',
        ]);
    }

    private function createDefaultRolePermissions()
    {
        $permissions = [
            ['name' => 'user.access'],
            ['name' => 'user.create'],
            ['name' => 'user.edit'],
            ['name' => 'user.view'],
            ['name' => 'user.delete'],

            ['name' => 'fee.access'],
            ['name' => 'fee.create'],
            ['name' => 'fee.edit'],
            ['name' => 'fee.view'],
            ['name' => 'fee.delete'],

            ['name' => 'department.access'],
            ['name' => 'department.create'],
            ['name' => 'department.edit'],
            ['name' => 'department.view'],
            ['name' => 'department.delete'],

            ['name' => 'transaction.access'],
            ['name' => 'transaction.delete'],

            ['name' => 'profile.access'],
            ['name' => 'profile.edit'],

            ['name' => 'settings.access'],
            ['name' => 'settings.edit'],

            ['name' => 'role_permission.access'],
            ['name' => 'role_permission.create'],
            ['name' => 'role_permission.edit'],
            ['name' => 'role_permission.view'],
            ['name' => 'role_permission.delete'],
        ];

        foreach ($permissions as $permission) {
            Permission::create([
                'name' => $permission["name"]
            ]);
        };

        //get all permissions via Gate::before rule; see AuthServiceProvider
        Role::create([
            'name' => 'Admin'
        ]);

        $userPermissions = [
            'profile.access',
            'profile.edit',
        ];

        Role::create([
            'name' => 'User'
        ])->givePermissionTo($userPermissions);
    }

    private function createDefaultUsers()
    {
        User::create([
            'name'              => 'admin',
            'email'             => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('123123123'),
            'remember_token'    => Str::random(10),
        ])->assignRole('Admin');
    }

    private function createDefaultFees()
    {
        Fee::create([
            'name'   => 'Post Graduate Acceptance Fee',
            'amount' => 30000,
        ]);
        Fee::create([
            'name'   => 'Undergraduate Acceptance Fee',
            'amount' => 25000,
        ]);
        Fee::create([
            'name'   => 'Centre for Educational Studies (CES) Form',
            'amount' => 15000,
        ]);
        Fee::create([
            'name'   => 'Post UTME Result Checker',
            'amount' => 2000,
        ]);
        Fee::create([
            'name'   => 'Centre for Educational Studies (CES) Result Checker',
            'amount' => 2000,
        ]);
        Fee::create([
            'name'   => 'Post Graduate Application',
            'amount' => 25000,
        ]);
    }

    private function createDefaultDepartments()
    {
        Department::create([
            'department_name' => 'English',
        ]);
        Department::create([
            'department_name' => 'Math',
        ]);
    }
}
