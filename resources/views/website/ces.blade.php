@extends('layouts.website')
@section('content')
<!-- ======== Main ======== -->
<div class="main">
  <div class="orderStatus">
    <ol class="d-flex p-0" style="list-style: none">
      <li class="status text-center bg-dark text-white py-1">1. <p class="d-none d-md-inline-block">Fee Pay</p>
      </li>
      <li class="status text-center bg-secondary text-white py-1">2. <p class="d-none d-md-inline-block">Review Order</p>
      </li>
      <li class="status text-center bg-secondary text-white py-1">3. <p class="d-none d-md-inline-block">Completed Order</p>
      </li>
    </ol>
  </div>
  <div class="container-fluid">
    <div class="row justify-content-center mt-5">
      <div class="col-md-5">
        <div class="orderForm">
          <form class="bg-dark p-3 rounded-3" action="{{ route('ces.store') }}" method="post">
            @csrf
            <div class="mb-4 orderTitle">
              <h6 class="text-white">CES Form Fee</h6>
            </div>
            <div class="mb-3">
              <input placeholder="Name of Student" name="student_name" type="text" class="form-control" id="student_name" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
              <input placeholder="Form Number / Admission Number" name="admission_number" type="text" class="form-control" id="admission_number" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
              <select class="form-select" name="department" aria-label="Default select example">
                <option selected>Department</option>
                @foreach ($departments as $department)
                <option value="{{ $department->id }}">Department of {{ $department->department_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="mb-1">
              <input placeholder="Phone Number" name="phone_number" id="phone_number" type="tel" class="form-control" aria-describedby="emailHelp" value="+234">
              <label for="phone_number" class="text-light mt-1 mb-0" style="font-size: 12px">(To receive Token via SMS , use this number format +23480XXXXXXXX)</label>
            </div>
            <div class="mb-3">
              <input placeholder="E-mail Address" name="email" type="email" class="form-control" id="email" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
              <input value="{{ $fee->amount }}" type="text" class="form-control" id="fee_id" disabled aria-describedby="emailHelp">
              <input type="hidden" value="{{ $fee->id }}" name="fee_id">
            </div>
            <button type="submit" class="btn btn-warning d-block mx-auto">Pay</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@include('layouts.includes.dashboard.footer')
@endsection
