@extends('layouts.website')

@section('content')
<div class="main">
  <div class="orderStatus">
    <ol class="d-flex p-0" style="list-style: none">
      <li class="status text-center bg-dark text-white py-1">1.  <p class="d-none d-md-inline-block">Fee Pay</p></li>
      <li class="status text-center bg-dark text-white py-1">2.  <p class="d-none d-md-inline-block">Review Order</p></li>
      <li class="status text-center bg-dark text-white py-1">3.  <p class="d-none d-md-inline-block">Completed Order</p></li>
    </ol>
  </div>
  <div class="container-fluid">
    <div class="row justify-content-center mt-5">
      <div class="col-md-5">
        <div class="review">
          <div class="reviewTitle bg-secondary border-1 border-white text-white p-3">
            <p>Confirmed Order</p>
          </div>
          <div class="yourOrder p-4">
            <h4 class="listTitle py-2">Your order Confirmed</h4>
            <p>Your Token: <b>{{ $reference }}</b></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('layouts.includes.dashboard.footer')
@endsection
