@extends('layouts.website')

@section('content')
  <div class="main">
    <div class="orderStatus">
      <ol class="d-flex p-0" style="list-style: none">
        <li class="status review-status text-center bg-secondary text-white py-1">1. <p class="d-none d-md-inline-block">Fee Pay</p>
        </li>
        <li class="status review-status text-center bg-dark text-white py-1">2. <p class="d-none d-md-inline-block">Review Order</p>
        </li>
        <li class="status review-status text-center bg-secondary text-white py-1">3. <p class="d-none d-md-inline-block">Completed Order</p>
        </li>
      </ol>
    </div>
    <div class="container-fluid">
      <div class="row justify-content-center mt-5">
        <div class="col-md-5">
          <div class="review">
            <div class="reviewTitle bg-secondary border-1 border-white text-white p-3">
              <p>Order Details</p>
            </div>
            <div class="yourOrder p-4">
              <h4 class="listTitle py-2">Your Order</h4>
              <ul class="list-group">
                <li class="list-group-item border-0 d-flex justify-content-between align-items-center">
                  Student Name:
                  <span class="">{{ $order->student_name }}</span>
                </li>
                <li class=" list-group-item border-0 d-flex justify-content-between align-items-center">
                  Form Number / Admission Number:
                  <span class="">{{ $order->admission_number }}</span>
                </li>
                <li class=" list-group-item border-0 d-flex justify-content-between align-items-center">
                  Department:
                  <span class="">{{ $order->department->department_name }}</span>
                </li>
                <li class=" list-group-item border-0 d-flex justify-content-between
                        align-items-center">
                  Phone Number:
                  <span class="">{{ $order->phone_number }}</span>
                </li>
                <li class=" list-group-item border-0 d-flex justify-content-between">
                  Email:
                  <span class="ps-3 text-break">{{ $order->email }}</span>
                </li>
                <li class=" list-group-item border-0 d-flex justify-content-between align-items-center">
                  Proceed to buy Token:
                  <span class="fw-bold">NGN {{ $order->fee->amount }}</span>
                </li>
              </ul>
            </div>
            <form id="paymentForm">
              {{-- <input type="text" name="user_id" id="user_id" value="{{ $order->postgrad->id }}"> --}}
              <input type="hidden" name="email-address" id="email-address" value="{{ $order->email }}">
              <input type="hidden" name="amount" id="amount" value="{{ $order->fee->amount }}">
              <button type="submit" class="btn btn-warning d-block mx-auto mb-3">Pay Now</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.includes.dashboard.footer')
@endsection

@push('script')
  <script src="https://js.paystack.co/v1/inline.js"></script>

  <script>
    const paymentForm = document.getElementById('paymentForm');
    paymentForm.addEventListener("submit", payWithPaystack, false);

    function payWithPaystack(e) {
      e.preventDefault();
      let handler = PaystackPop.setup({
        amount: document.getElementById("amount").value * 100, // in the lowest currency value - kobo, pesewas or cent
        email: document.getElementById("email-address").value,
        ref: '{{ $order->token }}',
        key: '{{ config('paystack.key') }}',
        currency: '{{ config('paystack.currency') }}',
        onClose: function() {
          alert('Window closed.');
        },
        onBankTransferConfirmationPending: function() {
          alert('Window closed.');
        },
        callback: function(response) {
          window.location = "{{ url('/') }}/complete_order?for={{ $page_name }}&reference=" + response.reference;
        }
      });
      handler.openIframe();
    }
  </script>
@endpush
