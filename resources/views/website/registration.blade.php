@extends('layouts.website')
@section('content')
<div class="main registration">
  <div class="bannerMain" style="background-image: url({{ asset('website/assets/images/bannerNew.jpeg') }});">
    <div class="container">
      <div class="row">
        <div class="bannerText text-white">
          <span class="element"></span>
        </div>
        <div class="col-md-5 bannerForm">
          <form class="bg-transparent rounded-3 mx-lg-5 p-lg-4" method="POST" action="{{ route('register') }}">

            <x-jet-validation-errors class="mb-4 text-danger" />

            @csrf
            <div class="mb-md-3 mb-1">
              <label for="exampleInputEmail1" class="form-label text-white">Full Name</label>
              <input placeholder="Full Name Here" type="text" name="name" class="form-control" aria-describedby="emailHelp">
            </div>
            <div class="mb-md-3 mb-1">
              <label for="exampleInputPassword1" class="form-label text-white">Email</label>
              <input placeholder="Email Here" type="email" name="email" class="form-control">
            </div>
            <div class="mb-md-3 mb-1">
              <label for="exampleInputPassword1" class="form-label text-white">Phone Number</label>
              <input placeholder="Phone Number Here" type="number" name="phone" class="form-control">
            </div>
            <div class="mb-md-3 mb-1">
              <label for="exampleInputPassword1" class="form-label text-white">Password</label>
              <input placeholder="Enter your Password" name="password" type="password" class="form-control">
            </div>
            <div class="mb-md-3 mb-1">
              <label for="exampleInputPassword1" class="form-label text-white">Confirm Password</label>
              <input placeholder="Password Again" name="password_confirmation" type="password" class="form-control">
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
            <div class="mt-4">
              <x-jet-label for="terms">
                <div class="flex items-center">
                  <x-jet-checkbox name="terms" id="terms" />

                  <div class="ml-2">
                    {!! __('I agree to the :terms_of_service and :privacy_policy', [
                    'terms_of_service' => '<a target="_blank" href="' . route('terms.show') . '" class="underline text-sm text-gray-600 hover:text-gray-900">' . __('Terms of Service') . '</a>',
                    'privacy_policy' => '<a target="_blank" href="' . route('policy.show') . '" class="underline text-sm text-gray-600 hover:text-gray-900">' . __('Privacy Policy') . '</a>',
                    ]) !!}
                  </div>
                </div>
              </x-jet-label>
            </div>
            @endif
            <button type="submit" class="btn btn-warning d-block ms-auto mb-md-3 my-2">Register</button>
            <div class="formLink text-center">
              <a href="{{ url('/') }}" class="btn border-warning text-warning me-3">Back to Login</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@include('layouts.includes.dashboard.footer')
@endsection
