@extends('layouts.website')

@section('content')
  <div class="main">
    <div class="bannerMain" style="background-image: url({{ asset('website/assets/images/bannerNew.jpeg') }});">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="bannerText text-white mb-md-5">
              <span class="element"></span>
            </div>
          </div>

          @guest
            <div class="col-md-5 bannerForm">
              <form method="POST" action="{{ route('login') }}">

                <x-jet-validation-errors class="mb-4 text-danger" />
                @csrf
                <div class="mb-md-3 mb-1">
                  <label for="exampleInputEmail1" class="form-label text-white">Enter Your Email</label>
                  <input placeholder="Email Here" type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="mb-md-3 mb-1">
                  <label for="exampleInputPassword1" class="form-label text-white">Enter Your Password</label>
                  <input placeholder="Password" type="password" name="password" class="form-control" id="exampleInputPassword1">
                </div>
                <button type="submit" class="btn btn-warning d-block w-100 mb-md-3 my-2">Login</button>
                <div class="formLink text-center">
                  <a href="{{ url('registration') }}" class="btn border-warning text-warning">Register</a>
                </div>
                <a href="{{ route('password.request') }}" class="btn btn-link" style="color: #ffc107">Forgot your password?</a>
              </form>
            </div>
          @endguest

          @auth
            <div class="col-12">
              <div class="fee-div" style="position: relative; z-index: 11">
                <div class="row">
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('postGrad.create') }}">Post Graduate Acceptance Fee</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('underGrad.create') }}">Undergraduate Acceptance Fee</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('ces.create') }}">Centre for Educational Studies (CES) Form</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('utmeResult.create') }}">Post UTME Result Checker</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('cesResult.create') }}">Centre for Educational Studies (CES) Result Checker</a>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="shadow fee-item bg-white my-2 rounded-2">
                      <a class="p-md-4 px-2 py-3" style="color: #000; text-decoration: none; display: block" href="{{ route('postGradApplication.create') }}">Post Graduate Application</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endauth
        </div>
      </div>
    </div>
  </div>

  @include('layouts.includes.dashboard.footer')
@endsection
