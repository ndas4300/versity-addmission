@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom">
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">Undergraduate Acceptance Fee List</h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ url('dashboard/export-csv-file-underGrad/csv') }}" class="btn btn-success">Export CSV</a>
      </div>
    </div>
    <div class="card-body">
      <form method="get">
        <div class="row">
          <div class="col-md-3">
            <h4>Date from</h4>
            <input type="date" name="from" class="form-control" value="{{ request()->get('from') }}" id="datefilterfrom" data-date-split-input="true">
          </div>
          <div class="col-md-3">
            <h4>Date to</h4>
            <input type="date" name="to" class="form-control" value="{{ request()->get('to') }}" id="datefilterto" data-date-split-input="true">
          </div>
          <div class="col-md-3">
            <h4>Search By Date</h4>
            <input type="submit" value="Search" class="btn btn-primary">
          </div>
        </div>
      </form>

      <div class="table-responsive">
        <table id="testTable" class="table table-separate table-head-custom table-checkable" id="kt_datatable">
          <thead>
            <tr>
              <th>SL</th>
              <th>Name</th>
              <th>Admission Number</th>
              <th>Department</th>
              <th>Phone Number</th>
              <th>Email</th>
              <th>Amount</th>
              <th>Token</th>
              <th>Payment</th>
              <th>Date</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($underGrads as $underGrad)
              <tr>
                <td>
                  {{ $underGrad->sl }}
                </td>
                <td>
                  {{ $underGrad->student_name }}
                </td>
                <td>
                  {{ $underGrad->admission_number }}
                </td>
                <td>
                  {{ $underGrad->department->department_name }}
                </td>
                <td>
                  {{ $underGrad->phone_number }}
                </td>
                <td>
                  {{ $underGrad->email }}
                </td>
                <td>
                  {{ $underGrad->fee->amount }}
                </td>
                <td>
                  {{ $underGrad->token }}
                </td>
                <td>
                  @if ($underGrad->payment_status == 0)
                    <span class="badge badge-danger">Unpaid</span>
                  @else
                    <span class="badge badge-success">Paid</span>
                  @endif
                </td>
                <td>{{ $underGrad->created_at->format('d/m/Y') }}</td>

                <td nowrap="nowrap">
                  @can('transaction.delete')
                    <form action="{{ route('deleteUnderGradApplication', $underGrad->id) }}" method="post" id="delete_form">
                      @csrf
                      @method('DELETE')
                    </form>
                    <button class="btn btn-danger btn-sm" type="submit" form="delete_form">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
                        <path fill-rule="evenodd"
                          d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" />
                      </svg>
                    </button>
                  @endcan
                  @if ($underGrad->payment_status == 1)
                    <a href="{{ route('token_receipt', ['underGrad', $underGrad->id]) }}" class="ml-2">
                      <img src="https://img.icons8.com/color/48/000000/pdf-2--v2.png" height="25px" />
                    </a>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{ $underGrads->links() }}
    </div>
  </div>

@endsection
