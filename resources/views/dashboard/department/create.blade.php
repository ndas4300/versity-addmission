@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Add New Department
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('department.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
            <i class="ki ki-check icon-sm"></i>
            Save Form
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      <!--begin::Form-->
      <form class="form" id="kt_form" method="post" action="{{ route('department.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-xl-2"></div>
          <div class="col-xl-8">
            <div class="my-5">

              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="department_name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="department_name" id="department_name" value="{{ old('department_name') }}" class="form-control form-control-solid @error('department_name') is-invalid @enderror" type="text" required>
                  @error('department_name')
                  <div class="invalid-departmentdback">{{ $message }}</div>
                  @enderror
                </div>
              </div>


            </div>
          </div>
          <div class="col-xl-2"></div>
        </div>
      </form>
      <!--end::Form-->
    </div>
  </div>

@endsection
