@extends('layouts.dashboard')

@section('content')

  <div class="d-flex flex-row">
    <div class="flex-row-fluid ml-lg-8">
      <div class="card card-custom card-stretch">
        <div class="card-header py-3">
          <div class="card-title align-items-start flex-column">
            <h3 class="card-label font-weight-bolder text-dark">Edit User Information</h3>
          </div>
          <div class="card-toolbar">
            <button type="submit" class="btn btn-success mr-2" form="profile_update_form">Save Changes</button>
            <button type="reset" class="btn btn-secondary" form="profile_update_form">Cancel</button>
          </div>
        </div>

        <div class="px-8 mt-2">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif
        </div>

        <form class="form" action="{{ route('user.update', $user->id) }}" id="profile_update_form" method="post" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="card-body">
            <div class="form-group row">
              <label class="col-xl-3 col-lg-3 col-form-label" for="name">Name</label>
              <div class="col-lg-9 col-xl-6">
                <input class="form-control form-control-lg form-control-solid" type="text" id="name" name="name" value="{{ old('name', $user->name) }}">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-xl-3 col-lg-3 col-form-label" for="phone">Phone</label>
              <div class="col-lg-9 col-xl-6">
                <input type="text" class="form-control form-control-lg form-control-solid" id="phone" name="phone" value="{{ old('phone', $user->phone) }}" placeholder="Phone">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-xl-3 col-lg-3 col-form-label" for="email">Email Address</label>
              <div class="col-lg-9 col-xl-6">
                <input type="email" class="form-control form-control-lg form-control-solid" id="email" name="email" value="{{ old('email', $user->email) }}" placeholder="Email">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
  <script src="{{ asset('assets/dashboard/js/pages/crud/file-upload/image-input.js') }}"></script>
  <script>
    let avatar5 = new KTImageInput('user_image');
  </script>
@endpush
