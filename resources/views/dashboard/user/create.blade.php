@extends('layouts.dashboard')

@section('content')

  <div class="card card-custom card-sticky" id="kt_page_sticky_card">
    <div class="card-header">
      <div class="card-title">
        <h3 class="card-label">
          Add New User
        </h3>
      </div>
      <div class="card-toolbar">
        <a href="{{ route('user.index') }}" class="btn btn-light-primary font-weight-bolder mr-2">
          <i class="ki ki-long-arrow-back icon-sm"></i>
          Back
        </a>
        <div class="btn-group">
          <button type="submit" form="kt_form" class="btn btn-primary font-weight-bolder">
            <i class="ki ki-check icon-sm"></i>
            Save Form
          </button>
        </div>
      </div>
    </div>
    <div class="card-body">
      <form class="form" id="kt_form" method="post" action="{{ route('user.store') }}">
        @csrf
        <div class="row justify-content-center">
          <div class="col-xl-8">
            <div class="my-5">
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="name">Name&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="name" id="name" value="{{ old('name') }}" class="form-control form-control-solid @error('name') is-invalid @enderror" type="text" required>
                  @error('name')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="email">Email&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="email" id="email" value="{{ old('email') }}" class="form-control form-control-solid @error('email') is-invalid @enderror" type="email" required>
                  @error('email')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="phone">Phone&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="phone" id="phone" value="{{ old('phone') }}" class="form-control form-control-solid @error('phone') is-invalid @enderror" type="text" required>
                  @error('phone')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="password">Password&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="password" id="password" value="{{ old('password') }}" class="form-control form-control-solid @error('password') is-invalid @enderror" type="password" required>
                  @error('password')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="password_confirmation">Password Confirmation&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <input name="password_confirmation" id="password_confirmation" value="{{ old('password_confirmation') }}" class="form-control form-control-solid @error('password_confirmation') is-invalid @enderror"
                         type="password" required>
                  @error('password_confirmation')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-form-label" for="role">Role&nbsp;<span class="text-danger">*</span></label>
                <div class="col-md-9">
                  <select name="role" id="role" class="form-control form-control-solid @error('role') is-invalid @enderror">
                    <option value="">Select Role</option>
                    @foreach($roles as $role)
                      <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                  </select>
                  @error('role')
                  <div class="invalid-feedback">{{ $message }}</div>
                  @enderror
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection
