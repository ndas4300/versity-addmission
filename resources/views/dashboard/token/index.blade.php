@extends('layouts.dashboard')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card ">
              <div class="card-header ">
              <div class="d-flex justify-content-between align-items-center">
                <h3 class="card-title">Bordered Table</h3>
                  <a href="{{route('token.create')}}" class="btn btn-primary">Create Token</a>
                </div>
              </div>

              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Tokens</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if($tokens->count())
                  @foreach($tokens as $token)
                    <tr>
                      <td>{{$token->id}}</td>
                      <td>{{$token->token}}</td>
                      <td><a href="#" class="btn btn-sm btn-primary"></a></td>
                       </tr>
                    @endforeach

                   @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>

    </section>

@endsection
