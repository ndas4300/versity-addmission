<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Token Receipt</title>

  <style>
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    body {
      text-align: center;
      font-family: sans-serif;
      background-color: #f6f6f6;
    }

    .container {
      width: 450px;
      max-width: 100%;
      margin: 30px auto;
      padding: 20px;
      background-color: #fff;
    }

    .text-left {
      text-align: left;
    }

    .text-center {
      text-align: center;
    }

    .text-right {
      text-align: right;
    }

    .headerLogo {
      padding-bottom: 10px;
      border-bottom: 1px solid #e8e9e0;
    }

    .headerLogo img {
      height: 100px;
    }

    .tokenTitle h4 {
      font-size: 22px;
      margin-bottom: 15px;
      margin-top: 15px;
    }

    .tokenTitle p {
      background-color: #ffc107;
      border: none;
      padding: 12px 25px;
      font-weight: bold;
      letter-spacing: 1px;
    }

    table {
      width: 100%;
      margin: 20px 0 40px;
    }

    table td {
      padding: 8px 0;
      border-bottom: 1px solid #e8e9e0;
    }

    table tr:last-child td {
      border: none;
    }

    footer {
      font-size: 15px;
    }
  </style>

</head>

<body>
  <div class="container">
    <div class="headerLogo">
      <img src="{{ asset('website/assets/images/logo.png') }}" alt="logo">
    </div>
    <div class="tokenTitle">
      <h4>Here is your Token:</h4>
      <p>{{ $order->token }}</p>
    </div>
    <div class="tokenDetails">
      <table>
        <tbody>
          <tr>
            <td class="text-left">Name:</td>
            <td class="text-right">{{ $order->student_name }}</td>
          </tr>
          <tr>
            <td class="text-left">Form/Admission Number:</td>
            <td class="text-right">{{ $order->admission_number }}</td>
          </tr>
          <tr>
            <td class="text-left">Department:</td>
            <td class="text-right">{{ $order->department->department_name }}</td>
          </tr>
          <tr>
            <td class="text-left">Phone:</td>
            <td class="text-right">{{ $order->phone_number }}</td>
          </tr>
          <tr>
            <td class="text-left">Email:</td>
            <td class="text-right">{{ $order->email }}</td>
          </tr>
          <tr>
            <td class="text-left">Amount Paid:</td>
            <td class="text-right">{{ $order->fee->amount }}</td>
          </tr>
          <tr>
            <td class="text-left">Item Paid For:</td>
            <td class="text-right">{{ $order->fee->name }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <footer>
      <p>Copyright University of Calabar</p>
    </footer>
  </div>
</body>

</html>
