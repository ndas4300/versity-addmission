<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="{{ Cache::get('settings')->meta_description ?? ''}}">
  <meta name="keywords" content="{{ Cache::get('settings')->meta_keywords ?? ''}}">
  <meta name="title" content="{{ Cache::get('settings')->meta_title ?? ''}}">
  <title>{{ Cache::get('settings')->name ?? 'University of Calabar' }}</title>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <link rel="stylesheet" href="{{ asset('website/assets/css/style.css') }}">
  @stack('style')
</head>

<body>
  <div class="wrapper">
    <section class="headerMain-section py-2">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-sm-2 col-1 col-md-1 d-flex  align-items-center justify-content-start">
            <button class="btn border-dark" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
              <i class="fas fa-bars"></i>
            </button>
          </div>

          <div class="headerTop-logo col-5 col-md-2 d-flex  align-items-center justify-content-center">
            <a href="{{ url('/') }}"> <img class="img-fluid" src="{{ asset('website/assets/images/logo.png') }}" alt="logo"></a>
          </div>

          {{-- <div class="col-5">
            <form class="">
              <div class=" input-group rounded-pill">
                <input class="form-control border-dark" type="search" placeholder="Search" aria-label="Search">
                <span class="input-group-text bg-dark border-dark text-white"><i class="fas fa-search"></i></span>
              </div>
            </form>
          </div> --}}

          @auth
          <div class="user-div col-5 col-md-3 d-flex  align-items-center justify-content-end">
            {{-- don't change this button tag --}}
            <button type="submit" form="logout_form" class="btn border-dark m-3 d-block"><i class="fas fa-sign-in-alt"></i> Logout</button>
            <a href="{{ route('dashboard') }}" class="btn btn-warning text-white border-warning m-3 d-block"><i class="fas fa-user-plus"></i> Dashboard</a>
          </div>
          <form action="{{ route('logout') }}" method="POST" id="logout_form">
            @csrf
          </form>
          @else
          <div class="user-div col-5 col-md-3 d-flex  align-items-center justify-content-end">
            <a href="{{ url('/') }}" class="btn border-dark m-3 d-block"><i class="fas fa-sign-in-alt"></i> Login</a>
            <a href="{{ route('registration.index') }}" class="btn btn-warning text-white border-warning m-3 d-block"><i class="fas fa-user-plus"></i> Register</a>
          </div>
          @endauth

        </div>
        <!-- toggle menu -->
        <div class="offcanvas offcanvas-start toggleMenu" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
          <div class="toggleMenu-logo">
            <div class="container">
              <div class="row">
                <div class="col-10">
                  <div class="w-50 mx-auto">
                    <img class="img-fluid" src="{{ asset('website/assets/images/logo.png') }}" alt="logo">
                  </div>
                </div>
                <div class="col-2">
                  <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
              </div>
            </div>
          </div>
          <ul class="list-group">
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('postGrad.create') }}">Post Graduate Acceptance Fee</a></li>
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('underGrad.create') }}">Undergraduate Acceptance Fee</a></li>
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('ces.create') }}">Centre for Educational Studies (CES) Form</a></li>
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('utmeResult.create') }}">Post UTME Result Checker</a></li>
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('cesResult.create') }}">Centre for Educational Studies (CES) Result Checker</a></li>
            <li class="list-group-item p-0"><a class="d-block text-decoration-none" href="{{ route('postGradApplication.create') }}">Post Graduate Application</a></li>
          </ul>
        </div>
      </div>
    </section>
    @yield('content')
  </div>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
  </script>
  <script src="https://unpkg.com/typeit@8.0.2/dist/index.umd.js"></script>
  <script>
    new TypeIt(".element", {
      strings: ["We care about you, complete your registration seamlessly.",
        " We have gone digital, no more physical scratch cards. Introducing, Token!",
        "At the comfort of your homes, you can purchase Token to complete your registration process.",
        "Purchasing Token is so simple, safe and instant!",
        "Purchase Token with your Bank App, USSD, Debit Cards, QR Code Etc."
      ],
      speed: 100,
      nextStringDelay: 2000,
      loop: true,
      cursor: false,
      breakLines: false
    }).go();
  </script>

  <script>
    var input = document.querySelector('input[type="tel"]');

    input.addEventListener("keydown", function() {
      var oldVal = this.value;
      console.log(oldVal);
      var field = this;
      console.log("funciona");

      setTimeout(function() {
        if (field.value.indexOf('+234') !== 0) {
          field.value = oldVal;
        }
      }, 1);
    });
  </script>
  @stack('script')
</body>

</html>
