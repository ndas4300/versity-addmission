<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\FeeController;
use App\Http\Controllers\Admin\NoticeController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\TokenController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Website\CesController;
use App\Http\Controllers\Website\CesResultController;
use App\Http\Controllers\Website\ContactMessageController;
use App\Http\Controllers\Website\HomeController;
use App\Http\Controllers\Website\PostGradApplicationController;
use App\Http\Controllers\Website\PostGradController;
use App\Http\Controllers\Website\StudentRegistrationController;
use App\Http\Controllers\Website\UnderGradController;
use App\Http\Controllers\Website\UtmeResultCheckerController;
use Illuminate\Support\Facades\Route;
use Spatie\MediaLibrary\MediaCollections\Models\Media as MediaAlias;

// cache clear
Route::get('reboot', function () {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    dd('Done');
});
Route::get('storageLink', function () {
    Artisan::call('storage:link');
    dd('Done');
});

Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('user', UserController::class);
    Route::resource('faq', FaqController::class)->except('index', 'show');
    Route::resource('contact-message', ContactMessageController::class)->except(['show', 'create', 'edit', 'update']);
    Route::resource('notice', NoticeController::class)->except('show');
    Route::resource('fee', FeeController::class)->except('create', 'store');
    Route::resource('token', TokenController::class);
    Route::resource('department', DepartmentController::class);

    Route::get('inactive-user/{user}', [UserController::class, 'inactiveUser'])->name('user.inactive');
    Route::get('active-user/{user}', [UserController::class, 'activeUser'])->name('user.active');
    Route::get('post-grad', [\App\Http\Controllers\Admin\PostGradController::class, 'index'])->name('post.grad.fee.list');
    Route::delete('deletePostgrads/{id}', [\App\Http\Controllers\Admin\PostGradController::class, 'destroy'])->name('deletePostgrads');
    Route::get('post-grad-application', [\App\Http\Controllers\Admin\PostGradApplicationController::class, 'index'])->name('post.grad.application.fee.list');
    Route::delete('deletePostGradApplication/{id}', [\App\Http\Controllers\Admin\PostGradApplicationController::class, 'destroy'])->name('deletePostGradApplication');
    Route::get('under-grad', [\App\Http\Controllers\Admin\UnderGradController::class, 'index'])->name('under.grad.fee.list');
    Route::delete('deleteUnderGradApplication/{id}', [\App\Http\Controllers\Admin\UnderGradController::class, 'destroy'])->name('deleteUnderGradApplication');
    Route::get('ces', [\App\Http\Controllers\Admin\CesController::class, 'index'])->name('ces.fee.list');
    Route::delete('deleteCes/{id}', [\App\Http\Controllers\Admin\CesController::class, 'destroy'])->name('deleteCes');
    Route::get('utme-result', [\App\Http\Controllers\Admin\UtmeResultCheckerController::class, 'index'])->name('utme.result.fee.list');
    Route::delete('deleteUtme/{id}', [\App\Http\Controllers\Admin\UtmeResultCheckerController::class, 'destroy'])->name('deleteUtme');
    Route::get('ces-result', [\App\Http\Controllers\Admin\CesResultController::class, 'index'])->name('ces.result.fee.list');
    Route::delete('deleteCesResult/{id}', [\App\Http\Controllers\Admin\CesResultController::class, 'destroy'])->name('deleteCesResult');
    Route::get('token-receipt/{for}/{id}', [\App\Http\Controllers\Admin\MakePdfController::class, 'makePdf'])->name('token_receipt');

    Route::get('edit-profile', [UserController::class, 'editProfile'])->name('edit.profile');
    Route::get('change_password', [UserController::class, 'change_password'])->name('change_password');
    Route::get('settings/company_settings', [SettingController::class, 'editCompanySetting'])->name('company.edit');
    Route::post('settings/company_setting', [SettingController::class, 'updateCompanySetting'])->name('company.update');

    Route::delete('remove-media/{media}', function (MediaAlias $media) {
        $media->delete();
        return back()->with('success', 'Media successfully deleted.');
    })->name('remove-media');

    // Role Permission
    Route::resource('developer/permission', PermissionController::class)->only('index', 'store');
    Route::get('role/assign', [RoleController::class, 'roleAssign'])->name('role.assign');
    Route::post('role/assign', [RoleController::class, 'storeAssign'])->name('store.assign');
    Route::resource('role', RoleController::class);

    Route::get('export-csv-file/{slug}', [\App\Http\Controllers\Admin\PostGradController::class, 'exportCSV']);
    Route::get('export-csv-file-PostGradApplication/{slug}', [\App\Http\Controllers\Admin\PostGradApplicationController::class, 'exportCSVPostGradApplication']);
    Route::get('export-csv-file-underGrad/{slug}', [\App\Http\Controllers\Admin\UnderGradController::class, 'exportCSVUnderGrad']);
    Route::get('export-csv-file-CES/{slug}', [\App\Http\Controllers\Admin\CesController::class, 'exportCSVCES']);
    Route::get('export-csv-file-CES-Result/{slug}', [\App\Http\Controllers\Admin\CesResultController::class, 'exportCSVCESResult']);
    Route::get('export-csv-file-utmeResult/{slug}', [\App\Http\Controllers\Admin\UtmeResultCheckerController::class, 'exportUTMEResultResult']);
});

Route::middleware('web')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('registration', StudentRegistrationController::class);
    Route::middleware(['auth'])->group(function () {
        Route::resource('postGrad', PostGradController::class)->only('create', 'store', 'show');
        Route::resource('postGradApplication', PostGradApplicationController::class)->only('create', 'store', 'show');
        Route::resource('underGrad', UnderGradController::class)->only('create', 'store', 'show');
        Route::resource('ces', CesController::class)->only('create', 'store', 'show');
        Route::resource('utmeResult', UtmeResultCheckerController::class)->only('create', 'store', 'show');
        Route::resource('cesResult', CesResultController::class)->only('create', 'store', 'show');
        Route::get('complete_order', [HomeController::class, 'completeOrder'])->name('complete.order');
    });
});
