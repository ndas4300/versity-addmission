<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_name'     => ['required', 'string', 'max:255'],
            'admission_number' => ['required', 'string', 'max:255'],
            'email'            => ['required', 'email', 'max:255'],
            'phone_number'     => ['required', 'numeric'],
            'department'       => ['required', 'string', Rule::exists('departments', 'id')],
            'fee_id'           => ['required', 'string', Rule::exists('fees', 'id')],
        ];
    }
}
