<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PostGrad;
use App\Models\PostGradApplication;
use App\Models\UnderGrad;
use App\Models\Ces;
use App\Models\CesResult;
use App\Models\UtmeResultChecker;
use App\Models\User;
use App\Models\Department;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::count();
        $departments = Department::count();
        $totalUnPaidFeePostGrad = PostGrad::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeePostGrad = PostGrad::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();
        $totalUnPaidFeePostGradApplication = PostGradApplication::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeePostGradApplication = PostGradApplication::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();
        $totalUnPaidFeeUnderGrad = UnderGrad::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeeUnderGrad = UnderGrad::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();
        $totalUnPaidFeeCes = Ces::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeeCes = Ces::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();
        $totalUnPaidFeeCesResult = CesResult::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeeCesResult = CesResult::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();
        $totalUnPaidFeeUtmeResultChecker = UtmeResultChecker::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 0)
            ->count();
        $totalPaidFeeUtmeResultChecker = UtmeResultChecker::query()
            ->when(auth()->user()->hasRole('User'), fn ($q) => $q->where('user_id', auth()->id()))
            ->where('payment_status', 1)
            ->count();

        $totalPaidFee = $totalPaidFeePostGrad + $totalPaidFeePostGradApplication + $totalPaidFeeUnderGrad + $totalPaidFeeCes + $totalPaidFeeCesResult + $totalPaidFeeUtmeResultChecker;
        $totalUnpaidFee = $totalUnPaidFeePostGrad + $totalUnPaidFeePostGradApplication + $totalUnPaidFeeUnderGrad + $totalUnPaidFeeCes + $totalUnPaidFeeCesResult + $totalUnPaidFeeUtmeResultChecker;

        return view('dashboard.dashboard', compact('users', 'departments', 'totalPaidFee', 'totalUnpaidFee'));
    }
}
