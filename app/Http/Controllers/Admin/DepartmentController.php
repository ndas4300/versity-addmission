<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('department.access');
        $departments = Department::orderBy('created_at', 'DESC')->paginate(20);
        $this->putSL($departments);
        return view('dashboard.department.index', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->checkPermission('department.create');
        return view('dashboard.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('department.create');

        $validated = $request->validate([
            'department_name' => ['required', 'string', 'max:255'],
        ]);

        Department::create($validated);

        return redirect()->route('department.index')->with('success', 'Department Created Successfully.');
    }

    public function edit(Department $department)
    {
        $this->checkPermission('department.edit');

        return view('dashboard.department.edit', compact('department'));
    }

    public function update(Request $request, Department $department)
    {
        $this->checkPermission('department.edit');

        $validated = $request->validate([
            'department_name' => ['required', 'string', 'max:255'],
        ]);

        $department->update($validated);

        return redirect()->route('department.index')->with('success', 'Department updated successfully.');
    }

    public function destroy(Department $department)
    {
        $this->checkPermission('department.delete');

        $department->delete();
        return back()->with('success', 'Department deleted successfully.');
    }
}
