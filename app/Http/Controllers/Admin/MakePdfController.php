<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ces;
use App\Models\CesResult;
use App\Models\PostGrad;
use App\Models\PostGradApplication;
use App\Models\UnderGrad;
use App\Models\UtmeResultChecker;
use Barryvdh\DomPDF\Facade as PDF;

class MakePdfController extends Controller
{
    public function makePdf($for, $id)
    {
        $order = collect();
        if ($for == 'postGrad') {
            $order = PostGrad::findOrFail($id);
        } else if ($for == 'ces') {
            $order = Ces::findOrFail($id);
        } elseif ($for == 'cesResult') {
            $order = CesResult::findOrFail($id);
        } elseif ($for == 'postGradApplication') {
            $order = PostGradApplication::findOrFail($id);
        } elseif ($for == 'underGrad') {
            $order = UnderGrad::findOrFail($id);
        } elseif ($for == 'utmeResultChecker') {
            $order = UtmeResultChecker::findOrFail($id);
        } else {
            abort(404);
        }

        $pdf = PDF::loadView('template.token_receipt', compact('order'));
        return $pdf->download('token_receipt.pdf');
    }
}
