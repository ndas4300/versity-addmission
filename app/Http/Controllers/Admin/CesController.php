<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Ces;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CesController extends Controller
{
    public function index()
    {
        $this->checkPermission('transaction.access');
        $cess = Ces::with('fee', 'department')
            ->when(request()->has('from') && request()->has('to'), function ($query) {
                $form = Carbon::parse(request()->from)->toDateString();
                $to = Carbon::parse(request()->to)->addDay()->toDateString();
                return $query->whereBetween('created_at', [$form, $to]);
            })
            ->when(auth()->user()->hasRole('User'), fn ($query) => $query->where('user_id', auth()->id()))
            ->latest()
            ->paginate(20);
        $this->putSL($cess);
        return view('dashboard.ces.index', compact('cess'));
    }

    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $this->checkPermission('transaction.delete');
        Ces::findOrFail($id)->delete();
        return back()->with("success", 'Deleted successfully.');
    }

    public function exportCSVCES($slug)
    {
        $fileName = 'report.csv';
        $tasks = Ces::with('fee', 'department')->get();
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        $columns = array('Student Name', 'Admission number', 'Department Name', 'Phone Number', 'Email', 'Amount', 'Token', 'Payment Status');
        $callback = function () use ($tasks, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($tasks as $task) {
                $row['Student Name']    = $task->student_name;
                $row['Admission number']    = $task->admission_number;
                $row['Department Name']    = $task->department->department_name;
                $row['Phone Number']    = $task->phone_number;
                $row['Email']    = $task->email;
                $row['Amount']    = $task->fee->amount;
                $row['Token']    = $task->token;
                $row['Payment Status']    = $task->payment_status > 0 ? "Paid" : "Unpaid";
                fputcsv($file, array($row['Student Name'], $row['Admission number'], $row['Department Name'], $row['Phone Number'], $row['Email'], $row['Amount'], $row['Token'], $row['Payment Status']));
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
