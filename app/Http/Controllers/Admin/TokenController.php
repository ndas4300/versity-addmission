<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tokens = Token::orderBy('created_at', 'DESC')->paginate(200);
        return view('dashboardtoken.index', compact('tokens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboardtoken.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $token = Token::create([
            'token' => $request->token
        ]);
        Session::flush('success', 'token created successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Token $token
     * @return Response
     */
    public function show(Token $token)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Token $token
     * @return Response
     */
    public function edit(Token $token)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Token $token
     * @return Response
     */
    public function update(Request $request, Token $token)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Token $token
     * @return Response
     */
    public function destroy(Token $token)
    {
        //
    }
}
