<?php

namespace App\Http\Controllers\Admin;

use App\Models\Fee;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class FeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->checkPermission('fee.access');
        $fees = Fee::orderBy('created_at', 'DESC')->paginate(20);
        $this->putSL($fees);
        return view('dashboard.fee.index', compact('fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->checkPermission('fee.create');
        return view('dashboard.fee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->checkPermission('fee.create');

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'amount' => ['required', 'integer'],
        ]);

        Fee::create($validated);

        return redirect()->route('fee.index')->with('success', 'Fee Created Successfully.');
    }

    public function edit(Fee $fee)
    {
        $this->checkPermission('fee.edit');

        return view('dashboard.fee.edit', compact('fee'));
    }

    public function update(Request $request, Fee $fee)
    {
        $this->checkPermission('fee.edit');

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'amount' => ['required', 'string'],
        ]);

        $fee->update($validated);

        return redirect()->route('fee.index')->with('success', 'Fee updated successfully.');
    }

    public function destroy(Fee $fee)
    {
        $this->checkPermission('fee.delete');

        $fee->delete();
        return back()->with('success', 'Fee deleted successfully.');
    }
}
