<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $this->checkPermission('user.access');
        $users = User::with('roles')->whereHas('roles', fn($query) => $query->where('roles.id', '!=', 1))->latest()->paginate($this->itemPerPage);
        $this->putSL($users);
        return view('dashboard.user.index', compact('users'));
    }

    public function create()
    {
        $this->checkPermission('user.create');
        $roles = Role::where('id', '!=', 1)->get();
        return view('dashboard.user.create', compact('roles'));
    }

    public function store(UserRequest $request): RedirectResponse
    {
        $this->checkPermission('user.create');
        $validated = $request->validated();
        $validated['password'] = \Hash::make($validated['password']);
        User::create($validated)->assignRole($validated['role']);
        return redirect()->route('user.index')->with('success', 'User created successfully.');
    }

    public function show($id)
    {
        //
    }

    public function edit(User $user)
    {
        $this->checkPermission('user.edit');
        return view('dashboard.user.edit', compact('user'));
    }

    public function update(Request $request, User $user): RedirectResponse
    {
        $this->checkPermission('user.edit');
        $validated = $request->validate([
            'name'  => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'. $user->id],
            'phone' => ['required', 'string', 'unique:users,phone,'. $user->id],
        ]);
        $user->update($validated);

        return redirect()->route('user.index')->with('success', 'User updated.');
    }

    public function destroy(User $user): RedirectResponse
    {
        $this->checkPermission('user.delete');
        $user->delete();
        return back()->with("success", 'Deleted successfully.');
    }

    public function inactiveUser(User $user): RedirectResponse
    {
        $this->checkPermission('user.edit');
        $user->update(['status' => 0]);
        return back()->with('status', "User Inactivated Successfully");
    }

    public function activeUser(User $user): RedirectResponse
    {
        $this->checkPermission('user.edit');
        $user->update(['status' => 1]);
        return back()->with('status', "User Activated Successfully");
    }

    public function editProfile()
    {
        $this->checkPermission('profile.edit');
        $user = auth()->user();
        return view('dashboard.user.edit-profile', compact('user'));
    }

    public function change_password()
    {
        $this->checkPermission('profile.edit');
        return view('dashboard.user.change_password');
    }
}
