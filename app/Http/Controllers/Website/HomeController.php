<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Ces;
use App\Models\CesResult;
use App\Models\PostGrad;
use App\Models\PostGradApplication;
use App\Models\UnderGrad;
use App\Models\UtmeResultChecker;
use App\Notifications\PaymentCompleteNotification;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function index()
    {
        return view('website.index');
    }

    public function completeOrder()
    {
        $for = request()->has('for') ? request()->get('for') : null;
        $reference = request()->has('reference') ? request()->get('reference') : null;

        if ($for == null || $reference == null) {
            return redirect('/')->with('error', 'Something was wrong.');
        }

        if ($for == 'postGrad') {
            $order = PostGrad::where('token', $reference)->first();
        } else if ($for == 'ces') {
            $order = Ces::where('token', $reference)->first();
        } elseif ($for == 'cesResult') {
            $order = CesResult::where('token', $reference)->first();
        } elseif ($for == 'postGradApplication') {
            $order = PostGradApplication::where('token', $reference)->first();
        } elseif ($for == 'underGrad') {
            $order = UnderGrad::where('token', $reference)->first();
        } elseif ($for == 'utmeResultChecker') {
            $order = UtmeResultChecker::where('token', $reference)->first();
        } else {
            $order = null;
        }

        if ($order) {
            $order->update([
                'payment_status' => 1
            ]);

            \Notification::route('mail', $order->email)->notify(new PaymentCompleteNotification($order));
            $this->sendMsg($order->token, $order->phone_number);

            return view('website.orderConfirmed', compact('reference'));
        }

        return redirect('/')->with('error', 'Something was wrong.');
    }

    public function sendMsg($token, $phone)
    {
//        $message = "Hi there, here is your Token: " . $token . " University of Calabar www.unical.edu.ng";
        $message = "Your Token: " . $token;
        $ownerEmail = 'unyime.dominic@yahoo.com';
        $subAccount = 'PLAY MASTER SUB ACCOUNT';
        $subAccountPass = 'NGN.12345';
        $sender = 'UNICAL';
        $sendTo = $phone;

        $response = Http::post('http://www.smslive247.com/http/index.aspx?cmd=sendquickmsg&owneremail=' . $ownerEmail . '&subacct=' . $subAccount . '&subacctpwd=' . $subAccountPass . '&message=' . $message . '&sender=' . $sender . '&sendto=' . $sendTo . '&msgtype=0');
        if ($response->successful()) {
            return true;
        }
        return false;
    }
}
