<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\StudentRegistration;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;

class StudentRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('website.registration');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //   dd($request->all());
        $studentRegistrations = StudentRegistration::create([
            'student_name'     => $request->student_name,
            'email'           => $request->email,
            'phone_number'     => $request->phone_number,
            'password'        => $request->password,
            'confirmPassword' => $request->confirmPassword,

        ]);
        Session::flush('success', 'token created successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param StudentRegistration $studentRegistration
     * @return Response
     */
    public function show(StudentRegistration $studentRegistration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param StudentRegistration $studentRegistration
     * @return Response
     */
    public function edit(StudentRegistration $studentRegistration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param StudentRegistration $studentRegistration
     * @return Response
     */
    public function update(Request $request, StudentRegistration $studentRegistration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param StudentRegistration $studentRegistration
     * @return Response
     */
    public function destroy(StudentRegistration $studentRegistration)
    {
        //
    }
}
