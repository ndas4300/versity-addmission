<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeRequest;
use App\Models\CesResult;
use App\Models\Department;
use App\Models\Fee;
use Illuminate\Http\Request;

class CesResultController extends Controller
{
    public function create()
    {
        $fee = Fee::findOrFail(5);
        $departments = Department::all();
        return view('website.cesResult', compact('departments', 'fee'));
    }

    public function store(FeeRequest $request): \Illuminate\Http\RedirectResponse
    {
        $cesResults = CesResult::create([
            'token'            => 'UC' . substr(md5(rand(1, 10) . time()), 0, 8),
            'user_id'          => auth()->id(),
            'student_name'     => $request->input('student_name'),
            'admission_number' => $request->input('admission_number'),
            'department_id'    => $request->input('department'),
            'phone_number'     => $request->input('phone_number'),
            'email'            => $request->input('email'),
            'fee_id'           => $request->input('fee_id'),
        ]);
        return redirect()->route('cesResult.show', $cesResults->id);
    }

    public function show(CesResult $cesResult)
    {
        $order = $cesResult;
        $page_name = 'cesResult';
        return view('website.reviewOrder', compact('order', 'page_name'));
    }
}
