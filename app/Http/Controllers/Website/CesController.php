<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeRequest;
use App\Models\Ces;
use App\Models\Department;
use App\Models\Fee;
use Illuminate\Http\Request;

class CesController extends Controller
{
    public function create()
    {
        $fee = Fee::findOrFail(3);
        $departments = Department::all();
        return view('website.ces', compact('departments', 'fee'));
    }

    public function store(FeeRequest $request): \Illuminate\Http\RedirectResponse
    {
        $ces = Ces::create([
            'token'            => 'UC' . substr(md5(rand(1, 10) . time()), 0, 8),
            'user_id'          => auth()->id(),
            'student_name'     => $request->input('student_name'),
            'admission_number' => $request->input('admission_number'),
            'department_id'    => $request->input('department'),
            'phone_number'     => $request->input('phone_number'),
            'email'            => $request->input('email'),
            'fee_id'           => $request->input('fee_id'),
        ]);
        return redirect()->route('ces.show', $ces->id);
    }

    public function show($id)
    {
        $order = Ces::findOrFail($id);
        $page_name = 'ces';
        return view('website.reviewOrder', compact('order', 'page_name'));
    }
}
