<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeRequest;
use App\Models\Department;
use App\Models\Fee;
use App\Models\UnderGrad;
use Illuminate\Http\Request;

class UnderGradController extends Controller
{
    public function create()
    {
        $fee= Fee::findOrFail(2);
        $departments = Department::all();
        return view('website.underGrad', compact('departments', 'fee'));
    }

    public function store(FeeRequest $request): \Illuminate\Http\RedirectResponse
    {
        $underGrad = UnderGrad::create([
            'token'            => 'UC' . substr(md5(rand(1, 10) . time()), 0, 8),
            'user_id'          => auth()->id(),
            'student_name'     => $request->input('student_name'),
            'admission_number' => $request->input('admission_number'),
            'department_id'    => $request->input('department'),
            'phone_number'     => $request->input('phone_number'),
            'email'            => $request->input('email'),
            'fee_id'           => $request->input('fee_id'),
        ]);
        return redirect()->route('underGrad.show', $underGrad->id);
    }

    public function show(UnderGrad $underGrad)
    {
        $order = $underGrad;
        $page_name = 'underGrad';
        return view('website.reviewOrder', compact('order', 'page_name'));
    }
}
