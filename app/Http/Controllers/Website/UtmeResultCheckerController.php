<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeRequest;
use App\Models\Department;
use App\Models\Fee;
use App\Models\UtmeResultChecker;
use Illuminate\Http\Request;

class UtmeResultCheckerController extends Controller
{
    public function create()
    {
        $fee = Fee::findOrFail(4);
        $departments = Department::all();
        return view('website.utmeResult', compact('departments', 'fee'));
    }

    public function store(FeeRequest $request): \Illuminate\Http\RedirectResponse
    {
        $utmeResults = UtmeResultChecker::create([
            'token'            => 'UC' . substr(md5(rand(1, 10) . time()), 0, 8),
            'user_id'          => auth()->id(),
            'student_name'     => $request->input('student_name'),
            'admission_number' => $request->input('admission_number'),
            'department_id'    => $request->input('department'),
            'phone_number'     => $request->input('phone_number'),
            'email'            => $request->input('email'),
            'fee_id'           => $request->input('fee_id'),
        ]);
        return redirect()->route('utmeResult.show', $utmeResults->id);
    }

    public function show($id)
    {
        $order = UtmeResultChecker::findOrFail($id);
        $page_name = 'utmeResultChecker';
        return view('website.reviewOrder', compact('order', 'page_name'));
    }
}
