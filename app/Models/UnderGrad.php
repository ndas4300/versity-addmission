<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnderGrad extends Model
{
    use HasFactory;
    protected $fillable = [
        'payment_status',
        'token',
        'user_id',
        'student_name',
        'admission_number',
        'department_id',
        'phone_number',
        'email',
        'fee_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function fee()
    {
        return $this->belongsTo(Fee::class);
    }
}
