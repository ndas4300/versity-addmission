<?php

namespace App\Models;

use App\Models\Ces;
use App\Models\PostGrad;
use App\Models\CesResult;
use App\Models\UnderGrad;
use App\Models\UtmeResultChecker;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'amount'
    ];

    public function ces()
    {
        return $this->hasMany(Ces::class);
    }

  public function cesResult()
    {
        return $this->hasMany(CesResult::class);
    }

  public function postGrad()
    {
        return $this->hasMany(PostGrad::class);
    }

  public function underGrad()
    {
        return $this->hasMany(UnderGrad::class);
    }

  public function utmeResultChecker()
    {
        return $this->hasMany(UtmeResultChecker::class);
    }
}
