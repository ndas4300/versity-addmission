<?php

namespace App\Exports;

use App\Models\PostGrad;
use Maatwebsite\Excel\Concerns\FromCollection;

class PostGradExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $PostGrads = PostGrad::all();
        // $data = array();
        //         foreach ( $PostGrads as $PostGrad ) {
        //             // $checkChildes = ChildCategoryTwo::whereChildCategoryId( $PostGrad->id )->count();
        //             // $hasChildes   = $checkChildes > 0 ? "Yes" : "No";

        //             $data[] = $PostGrad->sl;
        //             $data[] = $PostGrad->student_name;
        //             $data[] = $PostGrad->admission_number;
        //             $data[] = $PostGrad->department->department_name;
        //             $data[] = $PostGrad->phone_number;
        //             $data[] = $PostGrad->email;
        //             $data[] = $PostGrad->fee->amount;
        //             $data[] = $PostGrad->token;
        //             $data[] = $PostGrad->payment_status > 0 ? "Unpaid" : "Paid";
        //             // $data[] = $hasChildes;
        //         }
        return $PostGrads;
    }
}
